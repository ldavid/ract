const isArray = Array.isArray
const isObject = d => d !== null && typeof d === 'object' && !isArray(d)
const isElement = d => d instanceof HTMLElement
const isWrapped = d => d instanceof ValueWrapper
const rid = () => 'RID' + crypto.randomUUID()

HTMLElement.prototype.on = function(evt, callback) {
  this.addEventListener(evt, callback)
  return this
}
Array.prototype.react = function(callback) {
  if (!this.every(isWrapped)) {
    throw new Error('can only react on array of reactive elements')
  }
  const overwrap = new ValueWrapper(callback(...this))
  this.forEach(r => r.listen(() => overwrap.set(callback(...this))))
  return overwrap
}

function createElementFromHTML(htmlString) {
  const div = document.createElement('div')
  div.innerHTML = htmlString
  return div.firstElementChild
}

// return the element and attribute name that have attrValue
// TODO optimize
function byAttrVal(node, attrValue) {
  for (const name of node.getAttributeNames()) {
    if (node.getAttribute(name) === attrValue) {
      return [node, name]
    }
  }
  for (const child of node.children) {
    const res = byAttrVal(child, attrValue)
    if (res) return res
  }
  return undefined
}

const onRemove = []
new MutationObserver(function(mutations) {
  const removedNodes = mutations.reduce((acc,m) => acc.concat(...m.removedNodes), [])
  for (const o in onRemove) {
    if (removedNodes.includes(o.el)) {
      o.callback()
      // this.disconnect()
      removedNodes.slice(removedNodes.indexOf(o.el), 1)
    }
  }
}).observe(document.body, {childList: true})
function onElementRemoved(element, callback) {
  onRemove.push({el:element, callback:callback})
}

export function html(strings, ...values) {
  const swapEls = []
  const bindFuncs = []
  const bindAttrs = []

  function addSwapEl(v) {
    const uuid = rid()
    const el = html`<div id="${uuid}"></div>`
    swapEls.push([uuid, v])
    return el.outerHTML
  }

  function handleWrapped(v) {
    if (v.attr()) {
      const id = rid()
      bindAttrs.push([id,v])
      return id
    }

    if (typeof v.value === 'number' || typeof v.value === 'string') {
      console.log('can be disconnected', v.value)
      const el = document.createTextNode(v.value)
      const off = v.listen(wrapper => el.textContent = wrapper.value)
      onElementRemoved(el, () => {
        console.log("yourElement was removed!")
        off()
      })
      return addSwapEl(el)
    }

    if (isArray(v.value)) {
      const el = html`<div class="wrapper"></div>`
      function onUpdate(vw) {
        el.innerHTML = ''
        vw.value.forEach(e => el.appendChild(e.value))
      }
      v.listen(onUpdate)
      onUpdate(v)
      return addSwapEl(el)
    }

    if (isElement(v.value)) {
      // TODO copy this value if multiple els because can't have same parent
      const el = html`<div class="wrapper"></div>`
      v.listen(vw => render(vw.value, el))
      render(v.value, el)
      return addSwapEl(el)
    }

    debugger
    // return addSwapEl(v.getNewEl())
    throw new Error('not implemented')
  }

  const raw = String.raw({ raw: strings }, ...values.map(v => {
    if (v === undefined || v === null) return ''
    if (typeof v === 'function') {
      const id = rid()
      bindFuncs.push([id, v])
      return id
    }
    if (isArray(v)) {
      return v.map(addSwapEl).join('')
    }
    if (isElement(v)) {
      return addSwapEl(v)
    }
    if (isWrapped(v)) {
      return handleWrapped(v)
    }
    return v
  }))
  const ret = createElementFromHTML(raw)
  swapEls.forEach(([uuid, v]) => {
    // console.log(v.value)
    ret.querySelector('#'+uuid).replaceWith(v)
  })
  bindFuncs.forEach(([id,f]) => {
    const [el,attr] = byAttrVal(ret, id)
    el.addEventListener(attr.replace(/^on/, ''), f)
    el.removeAttribute(attr)
  })
  bindAttrs.forEach(([id,v]) => {
    const [el,attr] = byAttrVal(ret, id)
    // TODO class/style
    v.listen(v => el.setAttribute(attr, v.value))
  })
  // console.log(raw, ret)

  return ret
}
function unwrap(w) {
  if (isObject(w.value) && !isElement(w.value)) {
    const ret = {}
    for (const key in w.value) {
      ret[key] = unwrap(w.value[key])
    }
    return ret
  } else if (isArray(w.value)) {
    return w.value.map(unwrap)
  }
  return w.value
}
function ValueWrapper(value) {
  const listeners = []
  let attr = false

  const update = () => {
    for (const listener of listeners) {
      listener(this)
    }
  }
  this.set = (value) => {
    console.log('wrapper set', this.value, '>>', value)
    if (isWrapped(value)) {
      if (this.value === value.value) {
        update()
        return
      }

    }

    if (isObject(value) && !isElement(value)) {
      // TODO rewrap deeply and rebind listeners
      console.warn('Not recommended to set a reactive object, update values instead', value)
      this.value = value
    } else if (isArray(value)) {
      this.value = value.map(v => new ValueWrapper(v))
    } else {
      this.value = value
    }
    update()
  }
  this.listen = listener => {
    listeners.push(listener)
    return () => {
      const index = listeners.indexOf(listener)
      if (index > -1) {
        listeners.splice(index, 1)
      }
    }
  }
  this.react = (callback) => {
    // console.log('react', callback, this.value, callback(this.value))
    const overwrap = new ValueWrapper(callback(this.value))
    this.listen(vw => overwrap.set(callback(vw.value)))
    return overwrap
  }
  this.valueOf = () => this.value
  this.toString = this.valueOf
  this.attr = (b) => {
    if (b === undefined) return attr
    attr = b
    return this
  }
  this.unwrap = () => unwrap(this)

  this.set(value)

  if (isObject(value)) {
    if (!isWrapped(value) && !isElement(value)) { // TODO really? or maybe more in set func
      for (const key in value) {
        if (value.hasOwnProperty(key)) {
          value[key] = new ValueWrapper(value[key])
        }
      }
    }
    return new Proxy(this, {
      get(target, prop, receiver) {
        if (target.value[prop]) return target.value[prop]
        if (target[prop]) return target[prop]
        return Reflect.get(...arguments)
      },
      set(obj, prop, value) {
        obj.value[prop].set(value)
        return true
      }
    })
  }

  if (isArray(value)) {
    return new Proxy(this, {
      get: (target, prop, receiver) => {
        if (prop === 'isWrapped') return true
        if (prop in target.value) {
          if (typeof target.value[prop] === 'function') {
            if (prop === Symbol.iterator) {
              // return target.value[prop]
              const unwrapped = target.unwrap()
              return unwrapped[prop].bind(unwrapped)
            }
            if (prop === 'map') {
              return fn => target.react(d => d.map(fn))
            }
            if (prop === 'react') {
              if (!target.value.every(isWrapped)) {
                return target.react
              }
              // debugger
              return fn => {
                const overwrap = new ValueWrapper(fn(target.value))
                const setter = d => overwrap.set(fn(d.value)) // TODO recursive array
                target.value.forEach(v => v.listen(setter))
                target.listen(setter)
                return overwrap
              }
            }
            if (prop === 'slice') {
              return () => {
                return target.unwrap().slice(...arguments)
              }
            }
            return (...args) => {
              // TODO bind on copy value ? [...this.value] ?
              const ret = target.value[prop].bind(target.value)(...args)
              update()
              return ret
            }
          }
          return target.value[prop]
        }

        // console.log(target[prop], Reflect.get(...arguments))
        // debugger

        if (prop in target) return target[prop]
        return Reflect.get(...arguments)
      },
      set(obj, prop, value) {
        const copy = obj.unwrap()
        copy[prop] = value
        obj.set(copy)
        return true
      }
    })
  }

  return new Proxy(this, {
    set(obj, prop, value) {
      if (prop === 'value') {
        obj.set(value)
        return true
      }
      // if (prop === 'set') {
      //   obj.set(value)
      // }
      return false
    }
  })
}

export function render(node, parent = document.body) {
  parent.innerHTML = ''
  parent.appendChild(node)
}

export function reactive(...args) {
  const wrappedArgs = args.map(v => new ValueWrapper(v))
  return wrappedArgs.length === 1 ? wrappedArgs[0] : wrappedArgs
}
export function useState(v) {
  const wrapper = new ValueWrapper(v)
  return [wrapper, value => wrapper.set(value)]
}

export function rfor(n, fn) {
  let ret = []
  for (let i = 0; i < n; i++) {
    ret.push(fn(i))
  }
  return ret
}

// ressources
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals
// https://programmingwithmosh.com/javascript/javascript-wrapper-objects/
// https://pomb.us/build-your-own-react/
// https://codehike.org/docs/introduction
// https://dev.to/daslaf/an-introduction-to-reactive-programming-in-javascript-140a
// https://css-tricks.com/reactive-uis-vanillajs-part-1-pure-functional-style/
// https://www.choo.io/docs/stores
// preact
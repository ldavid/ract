import { html, reactive } from '../ract.js'

export function SArray() {
	const numbers = reactive([0,1,2])
	return html`
		<div>
			<ul>
				${numbers.map(i => html`<li>${i}</li>`)}
			</ul>
			<button name="push" onclick=${_ => numbers.push(numbers.length)}>push</button>
			<button onclick=${_ => numbers.pop()}>pop</button>
			<button onclick=${_ => numbers.shift()}>shift</button>
			<button onclick=${_ => numbers.unshift(numbers[0] - 1)}>unshift</button>
			<button onclick=${_ => numbers.splice(numbers.length/2, 1)}>splice</button>
			<button onclick=${_ => numbers[0] += 1}>numbers[0] += 1</button>
		</div>
	`
}
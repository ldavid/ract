import { html, reactive } from '../ract.js'

export function Counter() {
  const c = reactive(0)
  return html`
    <div>
      <h1>${c}</h1>
      <button onclick=${() => c.set(c+1)}>+1</button>
      <button onclick=${() => c.value-=1}>-1</button>
    </div>
  `
}
import { html, reactive } from '../ract.js'

export function SCounter() {
	const count = reactive(0)
	return html`
		<button onclick=${_ => count.value += 1}>
			Clicked ${count}
			${count.react(d => d <= 1 ? 'time' : 'times')}
		</button>
	`
}
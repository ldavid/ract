import { html, reactive } from '../ract.js'

export function ReactiveObject() {
  const author = reactive({
    name: 'John Doe',
    books: [
      'Vue 2 - Advanced Guide',
      'Vue 3 - Basic Guide',
      'Vue 4 - The Mystery'
    ]
  })
  // const publishedBooksMessage = author.react(d => d.books.length > 0 ? 'Yes' : 'No')
  const publishedBooksMessage = author.books.react(d => d.length > 0 ? 'Yes' : 'No')
  const hasPublishedBook = publishedBooksMessage.react(d => d === 'Yes' ? 'hasPublishedBook' : '').attr(true)
  const authorStyle = author.name.react(d => d === 'Hey ed' ? 'color:red;' : '').attr(true)
  setTimeout(_ => {
    // return
    // reactive object enable proxy setters
    author.name = 'Hey ed'
    // author.books = []
    // author.books.length = 0
    author.books[0] = 'Hello world'
  }, 3000)
  return html`
    <div>
      <h1 style="${authorStyle}">${author.name}</h1>
      <ul>
        ${author.books.map(b => html`<li>${b}</li>`)}
      </ul>
      <p>Has published books:</p>
      <span class="${hasPublishedBook}">${publishedBooksMessage}</span>
      <style>
        .hasPublishedBook {
          color:green;
        }
      </style>
    </div>
  `
}
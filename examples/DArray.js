import { html, reactive } from '../ract.js'

export function DArray() {
	const numbers = reactive([[0,1,2],[3,4,5],[6,7,8]])
	return html`
		<div>
			<ul>
				${numbers.map((arr,j) => html`
					<li>[${j}]<ul>
						${arr.map(i => html`
							<li>${i}</li>
						`)}
					</ul></li>
				`)}
			</ul>
			<button onclick=${_ => numbers[0] = [0, -1, -2]}>numbers[0] = [0, -1, -2]</button>
			<button onclick=${_ => numbers[1][0]++}>numbers[1][0]++</button>
		</div>
	`
}
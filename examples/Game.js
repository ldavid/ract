import { html, useState } from '../ract.js'

// REACT TIC-TAC-TOE https://react.dev/learn/tutorial-tic-tac-toe
function Square(value, onSquareClick) {
  return html`
    <button class="square" onclick=${onSquareClick}>${value}</button>
  `
}
function Board(xIsNext, squares, onPlay) {
  function handleClick(i) {
    if (calculateWinner(squares) || squares[i].value) {
      return
    }
    const previousSquares = squares.slice() // raw copy
    if (xIsNext()) {
      squares[i] = 'X'
    } else {
      squares[i] = 'O'
    }
    onPlay(previousSquares)
  }
  
  const status = squares.react(() => {
    const winner = calculateWinner(squares.unwrap())
    if (winner) {
      return 'Winner: ' + winner
    }
    return 'Next player: ' + (xIsNext() ? 'X' : 'O')
  })

  return html`
    <div>
      <div class="status">${status}</div>
      <div class="board-row">
        ${Square(squares[0], () => handleClick(0))}
        ${Square(squares[1], () => handleClick(1))}
        ${Square(squares[2], () => handleClick(2))}
      </div>
      <div class="board-row">
        ${Square(squares[3], () => handleClick(3))}
        ${Square(squares[4], () => handleClick(4))}
        ${Square(squares[5], () => handleClick(5))}
      </div>
      <div class="board-row">
        ${Square(squares[6], () => handleClick(6))}
        ${Square(squares[7], () => handleClick(7))}
        ${Square(squares[8], () => handleClick(8))}
      </div>
    </div>
  `
}
export function Game() {
  const [history, setHistory] = useState([Array(9).fill('')])
  const [currentMove, setCurrentMove] = useState(0)
  const xIsNext = _ => currentMove % 2 === 0
  const currentSquares = currentMove.react(cm => history[cm])//history[0]

  function handlePlay(previousSquares) {
    history.unshift(previousSquares)
    // const nextHistory = [...history.slice(0, currentMove + 1), nextSquares]
    // setHistory(nextHistory)

    // setCurrentMove(nextHistory.length - 1)
    currentMove.value++
  }

  const moves = history.map((squares, move) => {
    function jumpTo() { setCurrentMove(move) }
    let description
    if (move > 0) {
      description = 'Go to move #' + move
    } else {
      description = 'Go to game start'
    }
    return html`
      <li key="${move}">
        <button onclick=${jumpTo}>${description}</button>
      </li>
    `
  })

  return html`
    <div id="root">
      <div class="game-board">
        ${Board(xIsNext, currentSquares, handlePlay)}
      </div>
      <div class="game-info">
        <ol>${moves}</ol>
      </div>
    </div>
  `
}
function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ]
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i]
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a]
    }
  }
  return null
}
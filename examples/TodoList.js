
import { html, reactive } from '../ract.js'

export function TodoList() {
  const todos = reactive([])
  function addTodo(evt) {
    evt.preventDefault()
    const inputText = evt.target.elements.text
    // todos.push({text: inputText.value})
    todos.set([...todos, {text: inputText.value}])
    inputText.value = ''
  }
  return html`
    <form onsubmit=${addTodo}>
      <label>
        <span>Add Todo</span>
        <input name="text" type="text" />
      </label>
      <button type="submit">Add</button>
      <ul>
        ${todos.map(todo => html`
          <li>${todo.text}</li>
        `)}
      </ul>
      <button type="button" onclick=${() => todos[0].text.value = 'Swaped'}>Swap name</button>
    </form>
  `
}
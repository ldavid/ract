import { html } from '../ract.js'

export function ShoppingList(products) {
  return html`
    <div>
      <style>
        .product { color: darkgreen; }
        .product.fruit { color: magenta; }
      </style>
      <ul>
        ${products.map(product => html`
          <li key="${product.id}" class="product ${product.isFruit ? 'fruit' : ''}">
            ${product.title}
          </li>
        `)}
      </ul>
    </div>
  `
}
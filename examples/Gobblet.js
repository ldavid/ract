import { html, reactive, rfor } from '../ract.js'

function Player(id, player) {
  return html`
    <div class="player">
        ${player.map((gobblet,i) =>
          Piece(
            [id, gobblet],
            e => {
              selection.set([player, gobblet, _ => players[playerId].set()])
            }
          )
        )}
    </div>
  `
}

function Piece(gobblet, onClick) {
  console.log(gobblet[1].value)
  const [playerId, size] = gobblet
  return html`
    <div class="gobblet player${playerId} size${size.value}" onclick=${onClick}></div>
  `
}

function Square(value, onSquareClick) {
  console.log(value)
  // value can be empty array ([]) or filled with [player,gobbletSize]
  // console.log(value, value.length, value.at(-1))
  return html`
    <button class="square" onclick=${onSquareClick}>
      ${value.length === 0 ? '' : Piece(value.at(-1), console.log)}
    </button>
  `
}

function Board(board) {
  function handleClick(evt, i) {
    if (!selection.value) return
    board.set([
      ...board.value.slice(0, i),
      [selection.value],
      ...board.value.slice(i+1)
    ])

    // evt.target.appendChild(selection.value)

    console.log(selection.value)
    selection.set(null)
  }
  return html`
    <div class="board">
      ${rfor(4, col => html`
        <div class="board-row">
          ${/*rfor(4, row =>
            board.react(s => Square(s[row+col], evt => handleClick(evt, row+col)))
          )*/''}
          ${board.react(s => Square(s[0*4+col], evt => handleClick(evt, 0*4+col)))}
          ${board.react(s => Square(s[1*4+col], evt => handleClick(evt, 1*4+col)))}
          ${board.react(s => Square(s[2*4+col], evt => handleClick(evt, 2*4+col)))}
          ${board.react(s => Square(s[3*4+col], evt => handleClick(evt, 3*4+col)))}
        </div>
      `)}
    </div>
  `
}

export function Gobblet() {
  const selection = reactive(null)
  selection.on('update', d => {
    // document.querySelector('.gobblet.selected')?.classList.remove('selected')
    // if (d.value) d.value[2].classList.add('selected')
  })

  const board = reactive(Array(4*4).fill([]))
  const players = reactive([3,3,3], [3,3,3])
  return html`
    <div class="gobblet-game">
      ${/*Player(0, players[0])*/''}
      ${/*Board(board)*/''}
      ${/*Player(1, players[1])*/''}
      <style>${css}</style>
    </div>
  `
}

const css = /*css*/`
.gobblet-game {
  display: flex;
}

.gobblet {
  border-radius: 24px;
  margin: 4px;
}
.gobblet:hover {
  border: 1px grey solid;
}
.gobblet.selected {
  border: 1px white solid;
}

.player0 {
  background: red;
}
.player1 {
  background: blue;
}

.size1 {
  width: 16px;
  height: 16px;
}
.size2 {
  width: 24px;
  height: 24px;
}
.size3 {
  width: 32px;
  height: 32px;
}
`
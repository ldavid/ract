import { html, reactive } from '../ract.js'

export function CounterElegant() {
  const c = reactive(0)
  const cPlus1 = c.react(c => c+1)
  const test = { c2: reactive(0) }
  function cAdd2Mul2() { test.c2.set((test.c2+2)*2) }
  return html`
    <div>
      <h1>${c}</h1>
      <button onclick=${_ => c.set(c+1)}>${c}+1=${cPlus1}</button>
      <button onclick=${_ => c.set(c-1)}>-1</button>
      <h1>${test.c2}</h1>
      <button onclick=${cAdd2Mul2}>+2 *2</button>
    </div>
  `
}
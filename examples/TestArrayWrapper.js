import { html, useState } from '../ract.js'

export function TestArrayWrapper() {
  const [arr, setArr] = useState([1,2,3])
  function onClick() {
    setArr([...arr, arr.length+1])

    // arr.push(arr.length+1)
  }
  return html`
    <div>
      <ul>
        ${arr.map(e => html`<li>${e}</li>`)}
      </ul>
      <button onclick=${onClick}>+1</button>
    </div>
  `
}
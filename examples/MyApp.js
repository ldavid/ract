import { html, reactive } from '../ract.js'

// React first example
function MyButton(count, handleClick) {
  return html`<button onclick=${handleClick}>
    Clicked ${count} times
  </button>`
}
export function MyApp() {
  const count = reactive(0)
  const handleClick = () => count.value += 1
  return html`<div>
    <h1>Welcome to my app ${count.react(c => c * 2)}</h1>
    ${MyButton(count, handleClick)}
    ${MyButton(count, handleClick)}
  </div>`
}
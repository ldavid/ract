import { html, reactive } from '../ract.js'

export function TestDoubleDependency() {
  const [x,y] = reactive(0,0)
  const coordinates = [x,y].react((rx,ry) => `${rx},${ry}`)
  document.addEventListener('mousemove', evt => x.set(evt.clientX))
  document.addEventListener('click', evt => y.set(evt.clientY))
  return html`
    <div>
      <h1>${x}</h1>
      <h1>${y}</h1>
      <h1>${coordinates}</h1>
    </div>
  `
}
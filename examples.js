import { html, render, reactive } from './ract.js'

import { MyApp } from './examples/MyApp.js'
import { ShoppingList as ShopList } from './examples/ShoppingList.js'
import { Game } from './examples/Game.js'
import { TestArrayWrapper } from './examples/TestArrayWrapper.js'
import { TestDoubleDependency } from './examples/TestDoubleDependency.js'
import { Counter } from './examples/Counter.js'
import { CounterElegant } from './examples/CounterElegant.js'
import { ReactiveObject } from './examples/ReactiveObject.js'
import { TodoList } from './examples/TodoList.js'
import { Gobblet } from './examples/Gobblet.js'
import { SCounter } from './examples/SCounter.js'
import { SArray } from './examples/SArray.js'
import { DArray } from './examples/DArray.js'

const products = [
  { title: 'Cabbage', isFruit: false, id: 1 },
  { title: 'Garlic', isFruit: false, id: 2 },
  { title: 'Apple', isFruit: true, id: 3 },
]

const tabs = [
  MyApp,
  function ShoppingList() { return ShopList(products) },
  Game,
  TestArrayWrapper,
  TestDoubleDependency,
  Counter,
  CounterElegant,
  TodoList,
  // ReactiveObject,
  // Gobblet,
  SCounter,
  SArray,
  DArray
]

// ƒ µ $ Ʃ Ʌ ʘ ͱ

// TODO remove span wrapper for object with parent as wrapper
// TODO array deeply reactive
// TODO auto fragment (no need to <></> or fragment())
// TODO only render updated values in array
// TODO async/await
// TODO syntaxic sugar to manipulate data like it was mutable
// TODO remove listener for not displayed (unmounted) components

function Tabs() {
  const tab = reactive(localStorage.getItem('tab') ?? tabs.length-1)
  tab.listen(wrapper => localStorage.setItem('tab', wrapper.value))
  const currentTab = tab.react(d => tabs[d]())
  return html`
    <div>
      <nav>
        <ul>
          ${tabs.map((t,i) => html`
            <button onclick=${_ => tab.set(i)}>${t.name}</button>
          `)}
        </ul>
      </nav>
      ${currentTab}
    </div>
  `
}

render(Tabs())
# Ract
Tiny React without virtual DOM.

## How it works?
React, Vue, Svelte use the principle of reactive programming as you can also see inside very known software available to non-programmers as spreadsheet manipulation programs with formulas.

With `ract`, no need of JSX because template literal is used (``` html`` ```). You don't need to know anything else than pure html (no camelCase for attributes, ...). So you can just add `ract` as an import to use it directly.

`ract` encapsulate in proxy object reactive variables that call listeners when changes happens. Because `valueof` function is defined, you can use the variable directly when it's not a primitive type like boolean, string or number. In this case, you need to use `.value`.

The goal of `ract` is also to allow devs to manipulate data without thinking about the reactive manipulation. So, when you modify an array element of a reactive array, you can do it without some fonctional formalism and instead use your var like it was mutable. For example, you can `const arr = reactive([1,2,3]); arr.push(4)` and the value will launch the reactive chain.

## Reactive or useState
```js
import { html, reactive } from '../ract.js'

export function Counter() {
  const c = reactive(0)
  return html`
    <div>
      <h1>${c}</h1>
      <button onclick=${() => c.set(c+1)}>+1</button>
      <button onclick=${() => c.value-=1}>-1</button>
    </div>
  `
}
```

```js
import { html, useState } from '../ract.js'

export function Counter() {
  const [c, setC] = useState(0)
  return html`
    <div>
      <h1>${c}</h1>
      <button onclick=${() => setC(c+1)}>+1</button>
      <button onclick=${() => setC(c-1)}>-1</button>
    </div>
  `
}
```

## React - computed variables
Each time a reactive variable is updated, you can compute a new variable with the new value using `react` function.
```js
import { html, reactive } from '../ract.js'

export function CounterElegant() {
  const c = reactive(0)
  const cPlus1 = c.react(c => c+1)
  return html`
    <div>
      <h1>${c} + 1 = ${cPlus1}</h1>
      <button onclick=${() => c.value+=1}>+1</button>
      <button onclick=${() => c.value-=1}>-1</button>
    </div>
  `
}
```

## TODO
- [ ] remove listener for not displayed (unmounted) components
- [ ] remove span wrapper for object with parent as wrapper
- [ ] array deeply reactive
- [ ] auto fragment (no need to <></> or fragment())
- [ ] only render updated values in array
- [ ] async/await
- [ ] syntaxic sugar to manipulate data like it was mutable

## Ressources
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals
- https://programmingwithmosh.com/javascript/javascript-wrapper-objects/
- https://pomb.us/build-your-own-react/
- https://codehike.org/docs/introduction
- https://dev.to/daslaf/an-introduction-to-reactive-programming-in-javascript-140a
- https://css-tricks.com/reactive-uis-vanillajs-part-1-pure-functional-style/
- https://www.choo.io/docs/stores
- https://preactjs.com/
- 